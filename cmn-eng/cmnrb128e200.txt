-
Input sentence:He is getting better bit by bit.
Target sentence:他一點一點地變好。
Decoded sentence:他說謊。

-
Input sentence:I'm sorry, I don't have change.
Target sentence:對不起, 我沒有零錢。
Decoded sentence:我對他的勇氣感到驚訝。

-
Input sentence:I don't mind hot weather.
Target sentence:我不在乎炎熱的天氣。
Decoded sentence:我不想再要了。

-
Input sentence:Why can't you come?
Target sentence:你為什麼不能來?
Decoded sentence:你为什么长不？

-
Input sentence:It is said that his father died in a foreign country.
Target sentence:据说他爸爸在外国去世了。
Decoded sentence:說謊我錯誤的。

-
Input sentence:He is going to be a doctor when he grows up.
Target sentence:他长大了会成为一名医生。
Decoded sentence:他擅長足球。

-
Input sentence:Tom is swimming.
Target sentence:汤姆在游泳。
Decoded sentence:湯姆游泳游得非常快。

-
Input sentence:She becomes drowsy after dinner.
Target sentence:晚飯後她變得昏昏欲睡。
Decoded sentence:她一整天都沉默不语。

-
Input sentence:That's the village I was born in.
Target sentence:这就是我出生的村庄。
Decoded sentence:這就是你的想法。

-
Input sentence:I accepted the offer.
Target sentence:我接受了报价。
Decoded sentence:我接受了报价。

-
Input sentence:Some people criticized our decision.
Target sentence:一些人批评了我们的决定。
Decoded sentence:有些人永远也不是。

-
Input sentence:Gravity brings everything down to Earth.
Target sentence:重力将一切带下地球表面。
Decoded sentence:抓住汤姆。

-
Input sentence:Don't come into my room.
Target sentence:不要進入我的房間。
Decoded sentence:不要告訴我的訴任何人。

-
Input sentence:I love my wife.
Target sentence:我爱我的妻子。
Decoded sentence:我爱我的生命。

-
Input sentence:There's been a death in his family.
Target sentence:他的家不幸有人身亡。
Decoded sentence:的三个寂被无聊。

-
Input sentence:That was the first time that a man walked on the moon.
Target sentence:那是人类第一次在月球上行走。
Decoded sentence:那就是他的想法取。

-
Input sentence:They have twelve children.
Target sentence:他们有12个孩子。
Decoded sentence:他們有相同的嗜好。

-
Input sentence:I can't believe he did that.
Target sentence:我不能相信他做到了。
Decoded sentence:我無法相信我的右手臂。

-
Input sentence:Tom wants me to join his team.
Target sentence:汤姆希望我加入他的团队。
Decoded sentence:湯姆想要別的東西。

-
Input sentence:Do you know how to use this?
Target sentence:你知道怎么使用吗？
Decoded sentence:你知道他說是誰嗎?

-
Input sentence:Is it all right to take pictures in this building?
Target sentence:我们可以在大楼里拍照吗？
Decoded sentence:這真是能有一样的？

-
Input sentence:Wake up!
Target sentence:醒醒！
Decoded sentence:七。

-
Input sentence:Please keep this a secret.
Target sentence:请保密。
Decoded sentence:請保守這個秘密。

-
Input sentence:I need glue.
Target sentence:我需要胶水。
Decoded sentence:我需要个朋友。

-
Input sentence:He often drives his father's car.
Target sentence:他經常駕駛他的父親的車。
Decoded sentence:他常常彈吉他。

-
Input sentence:You're the best man for the job.
Target sentence:你是最適合做這份工作的人。
Decoded sentence:你有一天生活的。

-
Input sentence:The conference ended at five.
Target sentence:會議在五點鐘結束。
Decoded sentence:这个三周来来的很叫了。

-
Input sentence:Tom has blue eyes.
Target sentence:汤姆的眼睛是蓝色的。
Decoded sentence:湯姆完全筋疲力盡了。

-
Input sentence:The old man attempted to swim five kilometers.
Target sentence:老人试图游5公里。
Decoded sentence:這位老人下了公車。

-
Input sentence:I'd like to pay by credit card.
Target sentence:我想用信用卡支付。
Decoded sentence:我想要成为吉他手。

-
Input sentence:What are you concerned about?
Target sentence:你在担心什么呢？
Decoded sentence:你今天在看那個男孩嗎？

-
Input sentence:Did you miss me?
Target sentence:你想我了？
Decoded sentence:你见过汤姆了吗？

-
Input sentence:No arrests were ever made.
Target sentence:根本就没实施过逮捕行动。
Decoded sentence:没有必要的玛丽们下。

-
Input sentence:Where can I find them?
Target sentence:在哪儿我能找着他们？
Decoded sentence:在哪儿我能找到牙膏？

-
Input sentence:He made the most of his opportunity.
Target sentence:他盡力地利用了這個機會。
Decoded sentence:他做了一個書架給她。

-
Input sentence:I'm sleepy.
Target sentence:我困了。
Decoded sentence:我在這裡。

-
Input sentence:What number bus do I take to get to Waikiki?
Target sentence:我乘几路车去怀基基？
Decoded sentence:什麼时候給你這個做呢？

-
Input sentence:Why are you wearing that scarf?
Target sentence:你为什么戴那条围巾？
Decoded sentence:你今天为不道谁？

-
Input sentence:Prices went up.
Target sentence:物價上漲。
Decoded sentence:保持微笑。

-
Input sentence:I didn't think this was your seat.
Target sentence:我不認為這是你的座位。
Decoded sentence:我甚至沒考慮過你。

-
Input sentence:They didn't like you.
Target sentence:他们不喜欢你。
Decoded sentence:他们没听。

-
Input sentence:We gladly accept your offer.
Target sentence:我们很高兴接受你的提议。
Decoded sentence:我們為你們得親了。

-
Input sentence:We will reach London before dark.
Target sentence:我們會在天黑前到達倫敦。
Decoded sentence:我們讓他養狗和一隻狗。

-
Input sentence:Please come.
Target sentence:请来吧。
Decoded sentence:請我進來。

-
Input sentence:This place gives me a really bad vibe.
Target sentence:这地方给我一个很差的印象。
Decoded sentence:這個果汁喝起來酸酸的。

-
Input sentence:I objected to his paying the bill.
Target sentence:我反對他付帳單。
Decoded sentence:比汤姆说玛丽饿了解信。

-
Input sentence:What he said was beside the point.
Target sentence:他說的話跑題了。
Decoded sentence:他說的話真的傷害了我。

-
Input sentence:I can see the light.
Target sentence:我能看到光線。
Decoded sentence:我看见她正在锯一棵树。

-
Input sentence:Didn't you hear her speaking French?
Target sentence:你沒聽到她說法語嗎？
Decoded sentence:你們沒有任何學白就好嗎？

-
Input sentence:This is smaller than that.
Target sentence:这个比那个更小。
Decoded sentence:這完全是浪費時間。

-
Input sentence:Tom puts gas in his car twice a week.
Target sentence:湯姆每週幫車子加兩次油。
Decoded sentence:湯姆在學校裡上上你。

-
Input sentence:I think this machine is in need of repair.
Target sentence:我认为这机器需要修理。
Decoded sentence:我認為他不是故事的。

-
Input sentence:This car is easy to handle.
Target sentence:這輛車很容易操控。
Decoded sentence:這種飲料是免費招待的。

-
Input sentence:Get off the lawn!
Target sentence:離開草坪!
Decoded sentence:離開這裡。

-
Input sentence:I'd like to change yen to dollars.
Target sentence:我想把日元換成美元。
Decoded sentence:我想要一杯水。

-
Input sentence:I will explain it to her.
Target sentence:我会跟她解释的。
Decoded sentence:我会出席会议的。

-
Input sentence:He couldn't stand the bitterness of the coffee.
Target sentence:他受不了咖啡的苦味。
Decoded sentence:他無法得到這份工作。

-
Input sentence:I don't know when he will come.
Target sentence:我不知道他什么时候会来。
Decoded sentence:我不知道他能不能幫我做。

-
Input sentence:Our fence is made of iron.
Target sentence:我們的欄杆是鐵做的。
Decoded sentence:把我的时间到火。

-
Input sentence:I work with her boyfriend.
Target sentence:我和她的男朋友在一起上班。
Decoded sentence:我在一家工廠工作中。

-
Input sentence:Don't worry about such a thing.
Target sentence:不要擔心這樣的事情。
Decoded sentence:不要担心过这个。

-
Input sentence:Felicja likes to watch TV.
Target sentence:Felicja喜歡看電視。
Decoded sentence:歡迎留下來。

-
Input sentence:In the Edo period, moon-viewing parties were very popular.
Target sentence:在江戶時代賞月的宴會非常受歡迎。
Decoded sentence:這時候鐘起時間，的。

-
Input sentence:He should have arrived by now.
Target sentence:现在他应该已经到了。
Decoded sentence:他對女人沒有眼男孩孩。

-
Input sentence:When did she get married?
Target sentence:她什么时候结婚的？
Decoded sentence:什么时候开始下雨的?

-
Input sentence:I saw him running.
Target sentence:我看見了他跑步。
Decoded sentence:我看了一个女人。

-
Input sentence:English is spoken in Canada.
Target sentence:在加拿大的人說英語。
Decoded sentence:這裡的人不錯要。

-
Input sentence:I'd like to visit your country someday.
Target sentence:我想某一天拜访你的国家。
Decoded sentence:我想你的意見。

-
Input sentence:I got the money back from him.
Target sentence:我拿到了他還給我的錢。
Decoded sentence:我讓她清掃了我的房間。

-
Input sentence:Wet clothes stick to your skin.
Target sentence:濕衣服貼著你的皮膚。
Decoded sentence:歡迎到了最好的哭。

-
Input sentence:Everyone who knew him admired him.
Target sentence:每一個認識他的人都愛慕他。
Decoded sentence:沒有人在這裡工作。

-
Input sentence:Mary grabbed her purse and left.
Target sentence:Mary霸占了她的包包并且离开了。
Decoded sentence:瑪麗長得像她媽媽。

-
Input sentence:How much is that mountain bike?
Target sentence:那辆山地车多少钱？
Decoded sentence:它有多長時間？

-
Input sentence:I saw an old woman cross the street.
Target sentence:我看见一个老太太穿过了马路。
Decoded sentence:我看的一點就喜欢打了。

-
Input sentence:We spent the weekend with friends.
Target sentence:我們和朋友一起度過週末。
Decoded sentence:我们在公园里散步。

-
Input sentence:Do you need help?
Target sentence:你需要幫助嗎？
Decoded sentence:你真的吗？

-
Input sentence:Is this your book?
Target sentence:這是你的書嗎？
Decoded sentence:這是你的筆嗎？

-
Input sentence:She's absent because she's sick.
Target sentence:她不在是因为病了。
Decoded sentence:她正吃著的蘋果。

-
Input sentence:Maybe you should study harder next time.
Target sentence:你下回可能该更努力学习。
Decoded sentence:你可能該和是誰寫行。

-
Input sentence:Tell me again.
Target sentence:重新告訴我。
Decoded sentence:告訴我們多吃了。

-
Input sentence:I hugged him tightly and cried.
Target sentence:我緊緊地抱著他哭。
Decoded sentence:我決定一個醫生來。

-
Input sentence:I followed the deer's tracks.
Target sentence:我跟着鹿的踪迹。
Decoded sentence:我和朋友一样感冒。

-
Input sentence:I am looking forward to hearing from you soon.
Target sentence:我期待很快就能收到你的信。
Decoded sentence:我正在尋找工作中文。

-
Input sentence:Bring me a dry towel.
Target sentence:給我一條乾毛巾。
Decoded sentence:把它带给我。

-
Input sentence:You're too old to be doing this kind of thing.
Target sentence:你老得做不了这种事了。
Decoded sentence:你太年輕，不能退休。

-
Input sentence:The youngest daughter was particularly beautiful.
Target sentence:么女長得特別漂亮。
Decoded sentence:這房子正在燃手進。

-
Input sentence:I don't meet him so often.
Target sentence:我不常見到他。
Decoded sentence:我不在乎他做什麼。

-
Input sentence:This shirt costs ten dollars.
Target sentence:這襯衫要十元。
Decoded sentence:这只火鸡味道很好。

-
Input sentence:The rumor proved to be true.
Target sentence:经过证实，谣言是真的。
Decoded sentence:屋里挤满了人。

-
Input sentence:When water freezes it becomes ice.
Target sentence:水結凍後，變成冰。
Decoded sentence:我们需要的是帮助。

-
Input sentence:Whose fault is it?
Target sentence:是誰的錯呢？
Decoded sentence:這是誰的襯衫。

-
Input sentence:Do you think I'm crazy?
Target sentence:你认为我疯了吗？
Decoded sentence:你認為我傻嗎？

-
Input sentence:You do have choices.
Target sentence:你有选择。
Decoded sentence:你必须选择。

-
Input sentence:They have been married for ten years.
Target sentence:他們已經結婚十年了。
Decoded sentence:他們為甚麼都不會吃。

-
Input sentence:He is very good at playing violin.
Target sentence:他非常擅長拉小提琴。
Decoded sentence:他从中国回来的。

-
Input sentence:He passed the entrance examination.
Target sentence:他通過了入學考試。
Decoded sentence:他常常彈吉他。

-
Input sentence:Could you cancel my reservation?
Target sentence:你能取消我的預訂嗎？
Decoded sentence:你能重复一遍吗？

-
Input sentence:Do as you please.
Target sentence:你可以隨心所欲。
Decoded sentence:不是汤姆说的是么的。

-
Input sentence:Lots of famous people come here.
Target sentence:許多名人來這裡。
Decoded sentence:把收音機開大聲一點。

-
Input sentence:The storm didn't abate for several hours.
Target sentence:风暴几个小时没有减弱了。
Decoded sentence:這肉已經壞了。

batch_size = 128
epochs = 200
latent_dim = 256
num_samples = 10000
