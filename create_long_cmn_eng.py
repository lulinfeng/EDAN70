from __future__ import print_function
from collections import OrderedDict
import csv

ec_path = 'cmn-eng/cmn.txt'
ectest_path = 'cmn-eng/cmntest.txt'

cetrain_path ='long/cetrain.txt'
cetest_path='long/cetest.txt'

eng_path = 'cmn-eng/cmnpluseng.txt'
chi_path = 'cmn-eng/cmnpluschi.txt'
test_path = 'cmn-eng/test.txt'



cdic = {}
edic = {}
cedic = {}
cetestdic = {}
sorted_cedic={}
trainlines=[]
testlines=[]
cetest={}

englist=[]
chilist=[]

#write the original sentences


with open(ectest_path,'r', encoding='utf-8') as ft:
    lines = ft.read().split('\n')
    for line in lines[: len(lines) - 1]:
        eng, chi = line.split('\t')
        newline = chi+'\t'+ eng
        testlines.append(newline)
        cetest[chi]=eng

with open(cetest_path,'w') as fttr:
    for linetest in testlines:
        fttr.write(str(linetest) +'\n')

with open(ec_path,'r', encoding='utf-8') as f:
    lines = f.read().split('\n')
    for line in lines[: len(lines) - 1]:
        eng, chi = line.split('\t')
        if chi not in cetest.keys():
            newline = chi+'\t'+eng
            trainlines.append(newline)

with open(cetrain_path,'w') as ftr:
    for linetrain in trainlines:
        ftr.write(str(linetrain) +'\n')

#write the additional centences from book and web

with open(eng_path,'r', encoding='utf-8') as f:
    lines = f.read().split('\n')
    for line in lines[: len(lines)-1]:
        englist.append(line)

with open(chi_path,'r', encoding='utf-8') as f:
    lines = f.read().split('\n')
    for line in lines[: len(lines)-1]:
        chilist.append(line)

with open(cetrain_path, "a") as fa:
     for i in range(len(englist)):
         fa.write(str(chilist[i])+'\t'+ str(englist[i]) + '\n')

#write additional sentences from csv file

with open('long/sentences.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter='\t')
    for row in readCSV:
        if row[1]=='cmn':
            cdic[row[0]]=row[2]
        elif row[1]=='eng':
            edic[row[0]]=row[2]

with open('long/links.csv') as linkscsvfile:
    linksreadCSV = csv.reader(linkscsvfile, delimiter='\t')
    for row in linksreadCSV:
        if row[0] in cdic.keys() and row[1]in edic.keys():
            cedic[cdic[row[0]]]=edic[row[1]]

sorted_cedic = OrderedDict(sorted(cedic.items(), key=lambda t: len(t[0])))


with open(cetrain_path, "a") as fa:
    for k, v in sorted_cedic.items():
        fa.write(str(k)+'\t' + str(v) + '\n')





