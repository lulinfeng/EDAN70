from __future__ import print_function
import random
sc_path = 'swe-eng/swe.txt'
sctrain_path = 'swe-eng/swetrain.txt'
sctest_path = 'swe-eng/swetest.txt'
scdic = {}
stest = set()
trainlines=[]

with open(sc_path,'r', encoding='utf-8') as f:
    lines = f.read().split('\n')
for line in lines[: len(lines)-1]:
    swe, chi = line.split('\t')
    scdic[swe] = chi

with open(sctest_path,'w') as fte:
    a=0
    while a<20:
        # random.sample(), selecting 1 random element
        k= random.sample(scdic.keys(),1)[0]
        fte.write(str(k)+'\t'+str(scdic[k]) + '\n')
        stest.add(k)
        a=a+1

with open(sc_path,'r', encoding='utf-8') as f:
    lines = f.read().split('\n')
    for line in lines[: len(lines) - 1]:
        eng, chi = line.split('\t')
        if eng not in stest:
            trainlines.append(line)

with open(sctrain_path,'w') as ftr:
    for linetrain in trainlines:
        ftr.write(str(linetrain) +'\n')
