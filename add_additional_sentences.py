from __future__ import print_function


eng_path = 'cmn-eng/cmnpluseng.txt'
chi_path = 'cmn-eng/cmnpluschi.txt'

test_path = 'cmn-eng/test.txt'
train_path = 'cmn-eng/cmntrain.txt'

eng=[]
chi=[]

with open(eng_path,'r', encoding='utf-8') as f:
    lines = f.read().split('\n')
for line in lines[: len(lines)-1]:
    eng.append(line)

with open(chi_path,'r', encoding='utf-8') as f:
    lines = f.read().split('\n')
for line in lines[: len(lines)-1]:
    chi.append(line)

with open(test_path, "w") as fa:
    for i in range(len(eng)):
         fa.write(str(eng[i])+'\t'+ str(chi[i]) + '\n')


with open(train_path, "a") as fa:
     for i in range(len(eng)):
         fa.write(str(eng[i])+'\t'+ str(chi[i]) + '\n')

