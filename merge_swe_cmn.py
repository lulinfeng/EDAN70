from __future__ import print_function
from collections import OrderedDict
import random

swe_path = 'swe-cmn/swe.txt'
cmn_path = 'swe-cmn/cmn.txt'
sc_path = 'swe-cmn/sc.txt'
scl_path = 'swe-cmn/scl.txt'

esdic = {}
ecdic = {}
scdic = {}
sorted_scdic={}

with open(swe_path,'r', encoding='utf-8') as fs:
    slines = fs.read().split('\n')
for sline in slines[: len(slines)-1]:
    engs, swe = sline.split('\t')
    esdic[engs]=swe


with open(cmn_path,'r', encoding='utf-8') as fc:
    clines = fc.read().split('\n')
for cline in clines[: len(clines)-1]:
    engc, chi = cline.split('\t')
    ecdic[engc] = chi


for x in esdic.keys():
    for y in ecdic.keys():
         if x==y:
             scdic[esdic[x]] = ecdic[y]

#print(scdic)
#exit()

# dictionary sorted by length of the key string
sorted_scdic = OrderedDict(sorted(scdic.items(), key=lambda t: len(t[0])))

with open(sc_path,'w') as scf:
    for k, v in sorted_scdic.items():
        scf.write(str(k)+'\t' + str(v) + '\n')

with open(scl_path,'w') as scfl:
    for k, v in sorted_scdic.items():
        scfl.write(str(k)+'\t' + str(v) + '\n')
    a=0
    while a<16000:
        # random.sample(), selecting 1 random element
        kl = random.sample(sorted_scdic.keys(),1)[0]
        scfl.write(str(kl)+'\t'+str(sorted_scdic[kl]) + '\n')
        a=a+1



