from __future__ import print_function
import random

ec_path = 'cmn-eng/cmn.txt'
ectest_path = 'cmn-eng/cmntest.txt'
ectrain_path = 'cmn-eng/cmntrain.txt'

ecdic = {}
etest = set()
trainlines=[]

with open(ec_path,'r', encoding='utf-8') as f:
    lines = f.read().split('\n')
for line in lines[: len(lines)-1]:
    eng, chi = line.split('\t')
    ecdic[eng] = chi

with open(ectest_path,'w') as fte:
    a=0
    while a<100:
        # random.sample(), selecting 1 random element
        k= random.sample(ecdic.keys(),1)[0]
        fte.write(str(k)+'\t'+str(ecdic[k]) + '\n')
        etest.add(k)
        a=a+1

with open(ec_path,'r', encoding='utf-8') as f:
    lines = f.read().split('\n')
    for line in lines[: len(lines) - 1]:
        eng, chi = line.split('\t')
        if eng not in etest:
            trainlines.append(line)

with open(ectrain_path,'w') as ftr:
    for linetrain in trainlines:
        ftr.write(str(linetrain) +'\n')

