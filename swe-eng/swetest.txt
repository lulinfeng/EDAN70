Do you love your mother?	Älskar ni er mor?
Need I go on?	Behöver jag fortsätta?
I just need a little air.	Jag behöver bara lite frisk luft.
Did Tom pay, too?	Betalade Tom också?
Get a ticket for me.	Fixa en biljett åt mig.
Tom's living proof that you don't have to have brains to be successful.	Tom är levande bevis på att man inte behöver förstånd för att bli framgångsrik.
This yacht is very expensive.	Denna lustjakt är väldigt dyr.
I threw it out.	Jag slängde ut den.
I really want to know what's going on here.	Jag vill verkligen veta vad det är som pågår här.
I know that much.	Det där vet jag.
Tom has helped a lot.	Tom har varit till stor hjälp.
I can't find that setting.	Jag hittar inte den inställningen.
Tom is right-handed.	Tom är högerhänt.
Do you have the book?	Har du boken?
I want to eat something that I can only eat here.	Jag vill äta något som jag bara kan äta här.
Did Tom say yes?	Sa Tom ja?
Why do you hate Tom?	Varför hatar ni Tom?
We need Tom's help.	Vi behöver Toms hjälp.
Give Tom some time.	Ge Tom lite tid.
Tom studied law at Harvard.	Tom studerade juridik på Harvard.
